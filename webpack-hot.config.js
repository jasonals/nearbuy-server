var webpack = require('webpack');
var path = require('path');

// process.env.NODE_ENV = 'development';

var srcpath = JSON.stringify(path.join(__dirname, 'client'));

var debug = process.env.NODE_ENV !== 'production';

var config = {
  // profile: true,
  // debug: true,
  // bail: true,
  entry: {
    app: ['./client/app.js']
    , vendor: ['react', 'react-router', 'classnames', 'superagent', 'moment'],
  },
  output: {
    publicPath: 'http://0.0.0.0:8001/',
    // hotUpdateMainFilename: 'update/[hash]/update.json',
    // hotUpdateChunkFilename: 'update/[hash]/[id].update.js',

    path: path.join(__dirname, '/dist'),
    filename: 'dist/js/[name].js'
  },
  // externals: ['jquery'],
  // modulesDirectories: ['shared', 'node_modules'],
  eslint: {
    configFile: '.eslintrc',
    ignorePath: '.eslintignore',
  },
  module: {
    loaders: [
      {test: /\.js$/, loader: 'react-hot!babel-loader!eslint-loader', exclude: /node_modules/},
      // {test: /\.styl$/, loader: 'style-loader!css-loader!autoprefixer-loader!stylus-loader'},
      {test: /\.css$/, loader: 'style-loader!css-loader?sourceMap!autoprefixer-loader'},
      // {test: /\.(scss|sass)$/, loader: 'style-loader!css-loader!sass-loader'
      //   +'?includePaths[]=' + path.join(__dirname, 'src/styles')
      //   +'&includePaths[]=' + path.join(__dirname, 'node_modules')
      // },
      {test: /\.(scss|sass)$/, loader: 'style-loader!css-loader?module&localIdentName=[name]--[local]--[hash:base64:6]&sourceMap!autoprefixer-loader!sass-loader?outputStyle=expanded&sourceMapEmbed=true&sourceMapContents=true'
        +'&includePaths[]=' + path.join(__dirname, 'client/styles')
        +'&includePaths[]=' + path.join(__dirname, 'node_modules')
      },
      // {test: /\.json$/, loader: 'json-loader'},
      {test: /\.(jpg|jpeg|gif|png)$/, loader: 'url-loader'},
      {test: /\.svg($|\?)/, loader: 'url-loader'},
      // {test: /\.(jpg|jpeg|gif|png)$/, loader: 'url-loader?limit=100000&mimetype=image/png'},
      // {test: /\.(png|jpg|jpeg|gif)$/, loader: 'file-loader?name=[path][name].[ext]&limit=8192&context=./src'},
      // {test: /index\.jade$/, loader: 'file-loader?name=[name].html&context=./src!template-html-loader?raw'},
      {test: /\.(eot|ttf|woff|woff2)($|\?)/, loader: 'url-loader'}
    ]

  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json', '.scss']
    // root: [path.join(__dirname, 'src')]
  },
  devtool: 'source-map',

  plugins: [
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      DEBUG: debug,
      SRCPATH: srcpath
    })
    , new webpack.optimize.CommonsChunkPlugin('vendor', 'dist/js/vendor.bundle.js')

    , new webpack.HotModuleReplacementPlugin({
      // hotUpdateMainFilename: 'update/[hash]/update.json',
      // hotUpdateChunkFilename: 'update/[hash]/[id].update.js'
    })
    , new webpack.NoErrorsPlugin()
    // new webpack.NormalModuleReplacementPlugin(/\.(scss|sass)$/, 'node-noop')

  ],
};

module.exports = config;
