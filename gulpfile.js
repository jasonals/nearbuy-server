var gulp = require("gulp");
var webpack = require("webpack");
// var livereload = require("gulp-livereload");

gulp.task("prod", function(cb) {
  process.env.NODE_ENV = "production";
  var webpackConfig = require("./webpack.config.js");

  require("del")([webpackConfig.output.path]);

  // webpackConfig.output.filename = "js/[name].min.js";
  var build = webpack(webpackConfig);

  build.run(function(err, stats) {
    if (err) { console.log(err); }
    (stats.compilation.errors || []).forEach(function(err2){
      console.log(err2.message);
    });
    console.log("done build", new Date());
    if (cb) { cb(); }
  });

});


gulp.task("watch", function() {
  var WebpackDevServer = require("webpack-dev-server");
  var portfinder = require("portfinder");
  portfinder.basePort = 8001;

  process.env.NODE_ENV = "development";

  var webpackConfig = require("./webpack-hot.config.js");

  portfinder.getPort(function (err, port) {
    console.log(err, port);
    if (err) { console.log(err); return; }

    Array.prototype.unshift.apply(webpackConfig.entry.app, [
      "webpack-dev-server/client?http://0.0.0.0:" + port,
      "webpack/hot/only-dev-server"
    ]);

    new WebpackDevServer(webpack(webpackConfig), {hot: true}).listen(port, "0.0.0.0", function(err3) {
      if (err3) { console.log(err3); }
      console.log("Listening at 0.0.0.0:" + port);
    });

  });

});


gulp.task("dev", function(cb) {
  process.env.NODE_ENV = "development";

  var webpackConfig = require("./webpack.config.js");
  var build = webpack(webpackConfig);

  build.run(function(err, stats) {
    // console.log(err, stats);
    (stats.compilation.errors || []).forEach(function(err2){
      console.log(err2.message);
    });
    console.log("done build", new Date());
    if (cb) { cb(); }
  });

});
