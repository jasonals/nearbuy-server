import React from 'react';
import Router, {Location} from 'react-router';
// import Location from 'react-router/lib/Location';

// import webpackRequire from 'webpack-require';
//
// let routesRequire;
//
// webpackRequire(
//   require('../../webpack.config.js'),
//   require.resolve('../../client/routes.js'),
//   (err, factory, stats, fs) => {
//     if (err) {
//       console.error(err);
//     }
//
//     console.log(factory());
//     // routesRequire = factory();
//   }
// )


function generateHtml({html, title, init}) {
  let url = '';
  let styles = '<link rel="stylesheet" href="/dist/css/styles.css" type="text/css">';

  if (process.env.NODE_ENV !== 'production') {
    url = 'http://0.0.0.0:8001';
    styles = '';
  }

  return `
<!--
  @jasonals
-->
<html>
  <head>
    <title>${title}</title>
    ${styles}
  </head>
  <body>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1111924275489009',
          xfbml      : true,
          version    : 'v2.4'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
    <div id='fb-root'></div>
    <div id='app'>${html}</div>

    <script src='${url}/dist/js/vendor.bundle.js'></script>
    <script src='${url}/dist/js/app.js'></script>

  </body>
</html>
  `;
}

// const renderRoute = ({request, params={}, title, init, reply}, cb) => {
//   const location = new Location(request.url.path, request.url.query);
//
//   return Router.run(routes(), location, (error, state, transition) => {
//     let html = React.renderToString(<Router {...state} {...params} />);
//     console.log(html);
//     return reply( generateHtml({html, title, init}) );
//   });
// };


export function register(server, options, next) {

  [ '/', '/items',
    '/item/new',
    '/item/{id}',
    '/item/{id}/edit',
    '/user/{id}',
    '/login',
    '/about',
    '/rules-guidelines',
    '/admin',
    '/admin/users',
    '/admin/user/{id}',
    '/admin/items',
    '/admin/item/{id}',
    '/admin/ads',
    '/admin/ad/{id}',
    '/admin/permissions',
  ].map( path => {
    server.route({
      method: 'get',
      path: path,
      config: {
        handler: (request, reply) => {
          return reply( generateHtml({html: '', title: 'Marketplace'}) );
        },
      },
    });
  });

  // server.route({
  //   method: 'get',
  //   path: '/',
  //   config: {
  //     handler: (request, reply) => {
  //       return reply( generateHtml({html: '', title: 'Marketplace'}) );
  //     },
  //   },
  // });

  // server.route({
  //   method: 'get',
  //   path: '/',
  //   config: {
  //     description: 'Home',
  //     handler: (request, reply) => {
  //       console.log(request);
  //       // return reply( renderRoute({ title: 'Home', request, reply }) );
  //       return reply( generateHtml({html: '', title: 'Marketplace'}) );
  //     },
  //   },
  // });

}

register.attributes = {
  name: 'Client',
  version: '0.0.1',
};
