import Hapi from 'hapi';
import mongoose from 'mongoose';

const server = new Hapi.Server({
  connections: {
    routes: {
      cors: {
        headers: ['Authorization', 'Content-Type', 'If-None-Match', 'X-Requested-With'],
      },
    },
    router: {
      stripTrailingSlash: true,
    },
  },
});

server.connection({
  host: process.env.HOST || '0.0.0.0',
  port: process.env.PORT || 8000,
});

global.socketio = require('socket.io')(server.listener);

mongoose.connect(process.env.MONGODB_URL);

mongoose.connection.once('open', () => {
  server.log('info', 'Database connected!');
});

mongoose.connection.on('error', () => {
  server.log('error', 'Cannot connect to database!');
});

server.register(require('inert'), () => {});

server.route({
  method: 'GET',
  path: '/{name*}',
  handler: {
    directory: {
      path: 'public',
    },
  },
});


server.register({
  register: require('./client'),
}, (err) => err && server.log('error', err));

server.register({
  register: require('./auth'),
}, {
  routes: {
    prefix: '/users',
  },
}, (err) => err && server.log('error', err));

server.register({
  register: require('./graphql'),
}, (err) => err && server.log('error', err));

server.register({
  register: require('./graphiql'),
}, (err) => err && server.log('error', err));




server.register({
  register: require('good'),
  options: {
    reporters: [
      {
        reporter: require('good-console'),
        events: {
          response: '*',
          log: '*',
        },
      },
    ],
  },
}, (err) => err && server.log('error', err) );

server.register(require('vision'), err => err && console.log('Failed to load vision.') );

server.register({register: require('lout')}, (err) => err && server.log('error', err) );

server.start( () => {
  server.log('info', `Server running at: ${server.info.uri}`);
} );
