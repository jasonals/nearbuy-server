import {
  // GraphQLInterfaceType,
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLNonNull,
  // GraphQLSchema,
  GraphQLList,
  GraphQLString,
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLInt,
} from 'graphql';

import {
  UserModel,
  ItemModel,
  // AdModel,
} from '../models';

// function getProjection (fieldASTs) {
//   return fieldASTs.selectionSet.selections.reduce((projections, selection) => {
//     projections[selection.name.value] = 1;
//
//     return projections;
//   }, {});
// }

let itemPicture = new GraphQLObjectType({
  name: 'itemPicture',
  // description: 'A character in the Star Wars Trilogy',
  fields: () => ({

    pos: {
      type: GraphQLString,
    },
    url: {
      type: GraphQLString,
    },
  }),
});

// let userPicture = new GraphQLObjectType({
//   name: 'UserPicture',
//   // description: 'A character in the Star Wars Trilogy',
//   fields: () => ({
//     url: {
//       type: GraphQLString,
//     },
//   }),
// });

let adPicture = new GraphQLObjectType({
  name: 'adPicture',
  // description: 'A character in the Star Wars Trilogy',
  fields: () => ({
    url: {
      type: GraphQLString,
    },
  }),
});

export let ItemPictureInput = new GraphQLInputObjectType({
  name: 'ItemPictureInput',
  // description: 'A character in the Star Wars Trilogy',
  fields: {
    path: { type: GraphQLString },
    pos: { type: GraphQLString },
  },
});

export let AdPictureInput = new GraphQLInputObjectType({
  name: 'AdPictureInput',
  // description: 'A character in the Star Wars Trilogy',
  fields: {
    path: { type: GraphQLString },
  },
});

// export let userPictureInput = new GraphQLInputObjectType({
//   name: 'UserPictureInput',
//   // description: 'A character in the Star Wars Trilogy',
//   fields: {
//     path: { type: GraphQLString },
//   },
// });

export const itemType = new GraphQLObjectType({
  name: 'Item',
  description: 'Item information',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the user.',
      resolve: user => user._id,
    },
    owner: {
      type: userType,
      //   description: '',
      resolve: (item) => {
        return UserModel.findOne( {_id: item.owner} );
      },
    },
    description: {
      type: GraphQLString,
      description: '',
    },
    name: {
      type: GraphQLString,
      description: '',
    },
    active: {
      type: GraphQLBoolean,
    },
    created: {
      type: GraphQLInt,
      // description: '',
    },
    price: {
      type: GraphQLFloat,
      // description: '',
    },
    views: {
      type: GraphQLInt,
      // description: '',
    },
    pictures: {
      type: new GraphQLList(itemPicture),
      description: 'Pictures',
    },
  }),
});

export const userType = new GraphQLObjectType({
  name: 'User',
  description: 'User information',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the user.',
      resolve: user => user._id,
    },
    firstName: {
      type: GraphQLString,
      //   description: '',
    },
    lastName: {
      type: GraphQLString,
      // description: '',
    },
    email: {
      type: GraphQLString,
      // description: '',
    },
    picture: {
      type: GraphQLString,
      // description: '',
    },
    items: {
      type: new GraphQLList(itemType),
      resolve: (user) => ItemModel.find({owner: user.id}),
    },
    // password: {
    //   type: GraphQLString,
    //   description: '',
    // },
    joined: {
      type: GraphQLInt,
      description: 'Date joined',
    },
  }),
});

export const adType = new GraphQLObjectType({
  name: 'Ad',
  description: 'Ad information',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'The id of the ad.',
      resolve: ad => ad._id,
    },
    name: {type: GraphQLString},
    picture: {type: adPicture},
    link: {type: GraphQLString},
    active: {type: GraphQLBoolean},
    created: {type: GraphQLString},
    start: {type: GraphQLString},
    end: {type: GraphQLString},
  }),
});
