import {
  graphql,
} from 'graphql';

import _ from 'lodash';

import {schema} from './schema';
import deepParse from '../../helpers/deep-json-parse';

export function register(server, options, next) {

  server.route({
    method: 'post',
    path: '/graphql',
    config: {
      payload: {
        output: 'file',
        parse: true,
        uploads: 'temp',
      },
      description: 'GraphQL endpoint',
      handler: (request, reply) => {
        // console.log('payload', request.payload);
        let {query, variables, picture} = request.payload;
        // console.log(request.payload);
        let args = variables ? JSON.parse(variables) : null;
        // console.log(args, query);
        picture = _([picture])
        .flatten()
        .compact()
        // .map( ({filename, path}) => ({pos:}) )
        .value();

        if (picture.length) {
          args.pictures = [];
          let picPos = picture.length / 2;
          for (let i = 0; i < picture.length / 2; i++) {
            args.pictures[i] = {
              pos: +picture[i],
              path: picture[i + picPos].path,
            };
          }
        }

        // console.log(query, args);
        graphql(schema, query, null, args).then(result => {
          reply(result);
        });

      },
    },
  });

  return next();
}


register.attributes = {
  name: 'GraphQL',
  version: '0.0.1',
};
