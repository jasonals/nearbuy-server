import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLList,
  // GraphQLID,
  GraphQLString,
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLInt,
} from 'graphql';

import _ from 'lodash';
import gm from 'gm';
import fs from 'fs-extra';

import {
  UserModel,
  ItemModel,
  AdModel,
} from '../models';

import {userType, itemType, adType, ItemPictureInput, AdPictureInput} from './types';

import FB from 'fbgraph';
FB.setAppSecret('9b527f4318c6e30342095291ab66689d');

let basePath = `./public`;

fs.ensureDir(`${basePath}/imgs/items`, () => {})

function getProjection (source) {
  return source.fieldASTs[0].selectionSet.selections.reduce((projections, selection) => {
    projections[selection.name.value] = 1;

    return projections;
  }, {});
}

let query = new GraphQLObjectType({
  name: 'Query',
  fields: {
    user: {
      type: userType,
      args: {
        id: {
          name: 'id',
          type: GraphQLString,
        },
        email: {
          name: 'email',
          type: GraphQLString,
        },
      },
      resolve: (root, args, source) => {
        args._id = args.id;
        args.id = null;

        return UserModel.findOne( _.omit(args, _.isNull), getProjection(source));
      },
    },

    item: {
      type: itemType,
      args: {
        id: {
          name: 'id',
          type: GraphQLString,
        },
      },
      resolve: (root, args, source) => {
        args._id = args.id;
        args.id = undefined;

        return ItemModel.findOne( _.omit(args, _.isNull));
      },
    },

    users: {
      type: new GraphQLList(userType),
      args: {
      },
      resolve: (root, args, source) => {
        // console.log('test');

        return UserModel.find( {}, getProjection(source) );
      },
    },

    items: {
      type: new GraphQLList(itemType),
      args: {
        // id: {
        //   name: 'id',
        //   type: GraphQLString,
        // },
        owner: {
          name: 'owner id',
          type: GraphQLString,
        },
        active: {
          type: GraphQLBoolean,
        },
      },
      resolve: (root, args, source) => {
        // args._id = args.id;
        // args.id = null;
        // console.log(_.omit(args, _.isNull));
        // console.log('kkkkk', args, _.omit(args, _.isNull), getProjection(source));
        return ItemModel.find( _.omit(args, _.isNull), getProjection(source) );
      },
    },

    ads: {
      type: new GraphQLList(adType),
      args: {
        // id: {
        //   name: 'id',
        //   type: GraphQLString,
        // },
        owner: {
          name: 'owner id',
          type: GraphQLString,
        },
        active: {
          type: GraphQLBoolean,
        },
      },
      resolve: (root, args, source) => {
        // args._id = args.id;
        // args.id = null;
        // console.log(_.omit(args, _.isNull));
        // console.log('kkkkk', args, _.omit(args, _.isNull), getProjection(source));
        return AdModel.find( _.omit(args, _.isNull), getProjection(source) );
      },
    },

  },
});

let mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    newItem: {
      type: itemType,
      args: {
        description: {
          type: GraphQLString,
          // description: '',
        },
        name: {
          type: new GraphQLNonNull(GraphQLString),
          // description: '',
        },
        active: {
          type: GraphQLBoolean,
        },
        // picture: {
        //   type: GraphQLString,
        // },
        price: {
          type: GraphQLFloat,
          // description: '',
        },
        owner: {
          type: GraphQLString,
          // description: '',
        },
        sessionToken: {
          type: GraphQLString,
          // description: '',
        },
        pictures: {
          type: new GraphQLList(ItemPictureInput),
          // description: '',
        },
      },
      resolve: (root, args, source) => {
        let {sessionToken, pictures, ...fields} = args;
        fields = _.omit(fields, _.isNull);
        // console.log('save======', id, fields, pictures);
        // console.log(_.isEmpty(fields), 'oooo');

        let saved = new Promise( (resolve, reject ) => {
          let item = new ItemModel();

          for (let field in fields) {
            item[field] = fields[field];
          }
          item.owner = sessionToken;

          if (pictures) {
            pictures.forEach(picture => {
              let url = `/imgs/items/${item._id}-${(new Date()).getTime()}.jpg`;
              let savedPath = `${basePath}${url}`;

              item.pictures.push({pos: picture.pos, url });

              gm(fs.createReadStream(picture.path))
              .autoOrient()
              .resize(400, 400)
              .stream('jpg', function (err, stdout, stderr) {
                let writeStream = fs.createWriteStream(savedPath);
                stdout.pipe(writeStream);

                // stdout.on('end', () => resolve(item));
                // stdout.on('error', () => resolve(item));
              });

            });
            return resolve(item)
          }

          return resolve(item);

        });

        return saved.then( item => item.save() );
      },
    },
    editItem: {
      type: itemType,
      args: {
        id: {
          type: GraphQLString,
          // description: '',
        },
        description: {
          type: GraphQLString,
          // description: '',
        },
        name: {
          type: GraphQLString,
          // description: '',
        },
        active: {
          type: GraphQLBoolean,
        },
        price: {
          type: GraphQLFloat,
          // description: '',
        },
        sessionToken: {
          type: GraphQLString,
          // description: '',
        },
        pictures: {
          type: new GraphQLList(ItemPictureInput),
          // description: '',
        },
      },
      resolve: (root, args, source) => {
        let {id, pictures, ...fields} = args;
        fields = _.omit(fields, _.isNull);

        let saved = new Promise( (resolve, reject ) => {
          ItemModel.findOne({_id: id}, (err, item) => {
            if (err) {
              return reject(err);
            }

            for (let field in fields) {
              item[field] = fields[field];
            }

            if (pictures) {
              pictures.forEach(picture => {
                let exists = item.pictures.find( pic => +pic.pos === +picture.pos );
                let url = `/imgs/items/${item._id}-${(new Date()).getTime()}.jpg`;
                let savedPath = `${basePath}${url}`;

                if (exists) {
                  fs.unlink(`${basePath}${exists.url}`);
                  exists.url = url;
                } else {
                  item.pictures.push({pos: picture.pos, url });
                }

                gm(fs.createReadStream(picture.path))
                .autoOrient()
                .resize(400, 400)
                .stream('jpg', function (err, stdout, stderr) {
                  fs.unlink(picture.path);
                  let writeStream = fs.createWriteStream(savedPath);
                  stdout.pipe(writeStream);
                });

              });
            }
            return resolve(item);
          });

        });

        return saved.then( item => item.save() );
      },
    },
    newAd: {
      type: adType,
      args: {
        name: {type: GraphQLString},
        picture: {type: AdPictureInput},
        link: {type: GraphQLString},
        active: {type: GraphQLBoolean},
        start: {type: GraphQLString},
        end: {type: GraphQLString},
      },
      resolve: (root, args, source) => {
        let {sessionToken, picture, ...fields} = args;
        fields = _.omit(fields, _.isNull);
        // console.log('save======', id, fields, pictures);
        // console.log(_.isEmpty(fields), 'oooo');

        let saved = new Promise( (resolve, reject ) => {
          let ad = new AdModel();

          for (let field in fields) {
            ad[field] = fields[field];
          }
          ad.owner = sessionToken;

          if (picture) {
            let url = `/imgs/ads/${ad._id}-${(new Date()).getTime()}.jpg`;
            let savedPath = `${basePath}${url}`;

            ad.picture = { url };

            gm(fs.createReadStream(picture.path))
            .autoOrient()
            .resize(400, 400)
            .stream('jpg', function (err, stdout, stderr) {
              let writeStream = fs.createWriteStream(savedPath);
              stdout.pipe(writeStream);

              stdout.on('end', () => resolve(ad))
              stdout.on('error', () => resolve(ad))
            });

          } else {
            return resolve(ad);
          }

        });

        return saved.then( ad => ad.save() );
      },
    },
    editAd: {
      type: adType,
      args: {
        id: {type: GraphQLString},
        name: {type: GraphQLString},
        picture: {type: AdPictureInput},
        link: {type: GraphQLString},
        active: {type: GraphQLBoolean},
        start: {type: GraphQLString},
        end: {type: GraphQLString},

      },
      resolve: (root, args, source) => {
        let {id, picture, ...fields} = args;
        fields = _.omit(fields, _.isNull);

        let saved = new Promise( (resolve, reject ) => {
          AdModel.findOne({_id: id}, (err, ad) => {
            if (err) {
              return reject(err);
            }

            for (let field in fields) {
              ad[field] = fields[field];
            }

            if (picture) {
              let url = `/imgs/ads/${ad._id}-${(new Date()).getTime()}.jpg`;
              let savedPath = `${basePath}${url}`;

              if (ad.picture) {
                fs.unlink(`${basePath}${ad.picture.url}`);
                ad.picture.url = url;
              } else {
                ad.picture = { url };
              }

              gm(fs.createReadStream(picture.path))
              .autoOrient()
              .resize(400, 400)
              .stream('jpg', function (err, stdout, stderr) {
                let writeStream = fs.createWriteStream(savedPath);
                stdout.pipe(writeStream);


                stdout.on('end', () => {
                  fs.unlink(picture.path);
                  resolve(ad);
                })

                stdout.on('error', () => {
                  fs.unlink(picture.path);
                  resolve(ad);
                })
              });

            }
            return resolve(ad);
          });

        });

        return saved.then( ad => ad.save() );
      },
    },

    fblogin: {
      type: userType,
      args: {
        accessToken: {
          type: GraphQLString,
          // description: '',
        },
        signedRequest: {
          type: GraphQLString,
          // description: '',
        },
        userID: {
          type: GraphQLString,
          // description: '',
        },
      },
      resolve: (root, {accessToken, signedRequest, userID}, source) => {
        return new Promise( (resolve, reject ) => {

          FB.get('me', {access_token: accessToken, fields: 'email, first_name, last_name'}, (err, fbuser) => {
            // console.log(err, fbuser);

            UserModel.findOne({email: fbuser.email})
            .select(getProjection(source))
            .exec( (err, user) => {
              if (err) {
                return reject(err);
              }

              if (user) {
                return resolve(user);
              }

              FB.get('me/picture', {access_token: accessToken, width: 720}, (err, fbpicture ) => {
                return resolve(
                  UserModel.create({
                    email: fbuser.email,
                    firstName: fbuser.first_name,
                    lastName: fbuser.last_name,
                    picture: fbpicture.location,
                    active: true,
                  })
                );
              });

            });

          });
        });

      },
    },

  },
});



export let schema = new GraphQLSchema({
  query: query,
  mutation: mutation,
});
