
const generateHtml = () => {

  return `
    <!DOCTYPE html>
    <html>
      <head>
        <link rel="stylesheet" href="/graphiql/graphiql.css" />
        <script src="/graphiql/react.min.js"></script>
        <script src="/graphiql/fetch.min.js"></script>
        <script src="/graphiql/graphiql.min.js"></script>
      </head>
      <body>
        <div id='app'>Loading...</div>
        <script>

          /**
           * This GraphiQL example illustrates how to use some of GraphiQL's props
           * in order to enable reading and updating the URL parameters, making
           * link sharing of queries a little bit easier.
           *
           * This is only one example of this kind of feature, GraphiQL exposes
           * various React params to enable interesting integrations.
           */

          // Parse the search string to get url parameters.
          var search = window.location.search;
          var parameters = {};
          search.substr(1).split('&').forEach(function (entry) {
            var eq = entry.indexOf('=');
            if (eq >= 0) {
              parameters[decodeURIComponent(entry.slice(0, eq))] =
                decodeURIComponent(entry.slice(eq + 1));
            }
          });

          // if variables was provided, try to format it.
          if (parameters.variables) {
            try {
              parameters.variables =
                JSON.stringify(JSON.parse(query.variables), null, 2);
            } catch (e) {
              // Do nothing, we want to display the invalid JSON as a string, rather
              // than present an error.
            }
          }

          // When the query and variables string is edited, update the URL bar so
          // that it can be easily shared
          function onEditQuery(newQuery) {
            parameters.query = newQuery;
            updateURL();
          }

          function onEditVariables(newVariables) {
            parameters.variables = newVariables;
            updateURL();
          }

          function updateURL() {
            var newSearch = '?' + Object.keys(parameters).map(function (key) {
              return encodeURIComponent(key) + '=' +
                encodeURIComponent(parameters[key]);
            }).join('&');
            history.replaceState(null, null, newSearch);
          }

          // Defines a GraphQL fetcher using the fetch API.
          function graphQLFetcher(graphQLParams) {
            console.log('lll',graphQLParams);
            return fetch(window.location.origin + '/graphql', {
              method: 'post',
              body: JSON.stringify(graphQLParams),
            }).then(function (response) {
              console.log(response.json());
              return response.json()
            });
          }

          // Render <GraphiQL /> into the body.
          React.render(
            React.createElement(GraphiQL, {
              fetcher: graphQLFetcher,
              query: parameters.query,
              variables: parameters.variables,
              onEditQuery: onEditQuery,
              onEditVariables: onEditVariables
            }),
            document.getElementById('app')
          );
        </script>
      </body>
    </html>
  `;
};

export function register(server, options, next) {

  server.route({
    path: '/graphiql',
    method: 'get',
    handler: (request, reply) => {
      reply(generateHtml());
    },
  });

  return next();
}

register.attributes = {
  name: 'GraphiQL',
  version: '0.0.1',
};
