import Joi from 'joi';

import * as controller from './controllers';

export function register(server, options, next) {

  server.route({
    method: 'POST',
    path: '/register',
    config: {
      description: 'User register',
      handler: (request, reply) => {
        const {email, password} = request.payload;

        controller.register({email, password}, (err, user) => {
          reply(err || user);
        });

      },
      validate: {
        payload: {
          email: Joi.string().required(),
          password: Joi.string().required(),
        },
      },
    },
  });

  server.route({
    method: 'POST',
    path: '/login',
    config: {
      description: 'User login',
      handler: (request, reply) => {
        const {email, password} = request.payload;

        controller.login({email, password}, (err, user) => {
          reply(err || user);
        });

      },
      validate: {
        payload: {
          email: Joi.string().required(),
          password: Joi.string().required(),
        },
      },
    },
  });

  server.route({
    method: 'GET',
    path: '/',
    config: {
      description: 'User list',
      handler: (request, reply) => {
        controller.get({}, (err, users) => reply(err || users) );
      },
    },
  });

  server.route({
    method: 'post',
    path: '/getSessionInfo',
    config: {
      description: 'User Session Info',
      handler: (request, reply) => {
        const {sessionToken} = request.payload;

        controller.loginWithSessionToken({sessionToken}, (err, user) => reply(err || user) );
      },
      validate: {
        payload: {
          sessionToken: Joi.string().required(),
        },
      },

    },
  });

  return next();
}

register.attributes = {
  name: 'Auth',
  version: '0.0.1',
};
