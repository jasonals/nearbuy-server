import crypto from 'crypto';
import moment from 'moment';
import generateKey from 'password-generator';

import {UserModel, SessionModel} from '../models';

function hashPassword({password, email}, callback) {
  // https://nodejs.org/api/crypto.html#crypto_crypto_pbkdf2_password_salt_iterations_keylen_digest_callback
  // crypto.pbkdf2(password, salt, iterations, keylen[, digest], callback)
  crypto.pbkdf2(password, email, 10000, 64, 'sha512', function(err, key) {
    if (err){
      return callback && callback(err);
    }
    return callback && callback(null, key.toString('hex'));
  });
}

export function getSessionInfo({sessionToken}, callback) {

  let query = SessionModel.findOne({
    token: sessionToken,
    expire: { $gt: moment() },
  })
  .populate({
    path: 'user',
    select: 'email joined',
  })
  .select({
    _id: false,
    __v: false,
    password: false,
  });


  query.exec( (err, session) => {
    if (!session || err) {
      return callback && callback({ error: 'Access restricted.'});
    }
    return callback && callback(null, session);
  });

}


export function register({email, password}, callback) {

  if (!email || !password) {
    return callback && callback({err: 'Missing Field.'});
  }

// find
  UserModel.findOne({email: email})
  .exec( (err, user) => {
    if (user) {
      // TODO create error messages list somewhere
      return callback && callback({error: 'User already exists.'});
    }

    hashPassword({email, password}, (err, hashedPassword) => {
      //create
      let user = new UserModel({email: email, password: hashedPassword});
      //save
      user.save( (err) => {
        return callback && callback(err, user);
      });
    });

  });
}

export function login({email, password}, callback) {

  if (!email || !password) {
    return callback && callback({error: 'Missing Field.'});
  }

  UserModel.findOne({email: email})
  .exec( (err, user) => {
    // console.log(err, user);
    if (user) {
      // TODO login user

      hashPassword({password, email}, (err, hashedPassword) => {

        if (err) {
          return callback && callback(err);
        }

        if( hashedPassword === user.password) {
          let session = new SessionModel({
            user: user,
            token: generateKey(16, false, /[A-Za-z0-9]/),
            expire: moment().add(15, 'minutes'),
          });

          session.save( (err) => {
            return callback && callback(null, {
              sessionToken: session.token,
              expire: session.expire,
              email: user.email,
            });
          });

        }else{
          return callback && callback({error: 'Email or password does not match our records.'});
        }
      });


    } else {
      return callback && callback({error: 'Email or password does not match our records.'});
    }
  });
}

export function loginWithSessionToken({sessionToken}, callback) {
  getSessionInfo({sessionToken}, (err, session) => {
    if (err) {
      return callback && callback(err);
    }
    return callback && callback(null, {
      sessionToken: session.token,
      expire: session.expire,
      email: session.user.email,
    });

  });
}


export function get({email}, callback) {

  let query = UserModel.find()
  .select({
    _id: false,
    email: true,
    joined: true,
  });

  if (email) {
    query = query.where('email', email);
  }

  query.exec( (err, users=[]) => {
    // console.log(users);
    return callback && callback(err, users);
  });

}
