import mongoose, {Schema} from 'mongoose';

const schema = new Schema({
  expire: {type: Date, default: new Date() + (1000 * 5) },
  created: {type: Date, default: Date.now},
  token: {type: String, unique: true},
  user: {type: Schema.Types.ObjectId, ref: 'User'},
});

export const SessionModel = mongoose.model('Session', schema, 'session');
