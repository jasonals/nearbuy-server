import mongoose, {Schema} from 'mongoose';

const schema = new Schema({
  firstName: {type: String},
  lastName: {type: String},
  email: {type: String, unique: true},
  password: {type: String},
  active: {type: Boolean},
  // sessions: [{type: Schema.Types.ObjectId, ref: 'Session'}],
  joined: {type: Date, default: Date.now},
  picture: {type: String},
});

export default mongoose.model('User', schema, 'user');
