import mongoose, {Schema} from 'mongoose';

const schema = new Schema({
  owner: {type: Schema.Types.ObjectId, ref: 'User'},
  name: {type: String},
  price: {type: Number},
  description: {type: String},
  views: {type: Number},
  active: {type: Boolean},
  // sessions: [{type: Schema.Types.ObjectId, ref: 'Session'}],
  created: {type: Date, default: Date.now},
  pictures: [
    {
      url: {type: String},
      pos: {type: Number},
    },
  ],
});

export default mongoose.model('Item', schema, 'item');
