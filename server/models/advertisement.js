import mongoose, {Schema} from 'mongoose';

const schema = new Schema({
  name: {type: String},
  picture: {
    url: {type: String},
  },
  link: {type: String},
  active: {type: Boolean},
  created: {type: Date, default: Date.now},
  start: {type: Date},
  end: {type: Date},
});

export default mongoose.model('Ad', schema, 'ad');
