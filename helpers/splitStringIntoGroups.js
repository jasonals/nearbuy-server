export default function({string='', length = string.length || 0}) {

  return string.match(new RegExp('.{1,' + length + '}', 'g'));
}
