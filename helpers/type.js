export default function type(variable) {
  return Object.prototype.toString.call(variable).replace('[object ', '').replace(']', '').toLowerCase();
}
