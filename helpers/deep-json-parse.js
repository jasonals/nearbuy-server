import type from './type';

// deep JSON.parse
export default function parse(data) {
  if (type(data) === 'undefined') {
    return undefined;
  }
  if (type(data) === 'object') {
    return data;
  }

  return JSON.parse(data, (k, v) => {
    if (type(v) === 'array') {
      return v.map( val => parse(val));
    }

    return v;
  });
}
