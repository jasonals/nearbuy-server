var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
// var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');


// process.env.NODE_ENV = 'production';

var srcpath = JSON.stringify(path.join(__dirname, 'client'));

var debug = process.env.NODE_ENV !== 'production';

var config = {
  entry: {
    app: ['./client/app.js'],
    vendor: ['react', 'react-router', 'classnames', 'superagent', 'moment'],
  },
  output: {
    path: path.join(__dirname, 'public/dist'),
    publicPath: '/dist/',
    filename: debug ? 'js/[name].js' : 'js/[name].js',
  },
  // externals: ['jquery'],
  // modulesDirectories: ['shared', 'node_modules'],
  eslint: {
    configFile: '.eslintrc',
    ignorePath: '.eslintignore',
  },
  module: {
    loaders: [
      {test: /\.(js|jsx)$/, loader: 'babel-loader!eslint-loader', exclude: /node_modules/},
      {test: /\.css$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader?sourceMap!autoprefixer-loader')},
      {test: /\.(scss|sass)$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&localIdentName=[name]--[local]--[hash:base64:6]&sourceMap!autoprefixer-loader!sass-loader?outputStyle=expanded&sourceMapEmbed=true&sourceMapContents=true'
        + '&includePaths[]=' + path.join(__dirname, 'client/styles')
        + '&includePaths[]=' + path.join(__dirname, 'node_modules')
      )},
      // {test: /index(\-dev|\-prod).html$/, loader: 'file-loader?name=index.html'},
      {test: /\.(jpg|jpeg|gif|png)$/, loader: 'file-loader?name=/imgs/[name].[ext]'},
      {test: /\.svg($|\?)/, loader: 'file-loader?name=/imgs/[name]-[hash].[ext]'},
      {test: /\.(eot|ttf|woff|woff2)($|\?)/, loader: 'file-loader?name=/css/[name]-[hash].[ext]'},
    ],
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.json', '.scss'],
    // root: [path.join(__dirname, 'src')]
  },
  devtool: 'source-map',

  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      DEBUG: debug,
      SRCPATH: srcpath,
    }),
    new ExtractTextPlugin(debug ? 'css/styles.css' : 'css/styles.css', { allChunks: true }),
    new webpack.optimize.CommonsChunkPlugin('vendor', 'js/vendor.bundle.js'),
  ],
};

if (!debug) {
  Array.prototype.push.apply(config.plugins, [
    new webpack.optimize.UglifyJsPlugin({output: {comments: false}}),
    new webpack.optimize.OccurenceOrderPlugin(),
  ]);
}

module.exports = config;
