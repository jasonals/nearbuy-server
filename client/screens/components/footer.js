import React from 'react';

import styles from './footer.scss';


export default class Footer extends React.Component{

  render() {
    return (
      <div className={styles.container}>

        <div className={styles.row}>
          <ul className={styles.column}>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
          </ul>

          <ul className={styles.column}>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
            <li>Link</li>
          </ul>

        </div>

      </div>
    );
  }

}
