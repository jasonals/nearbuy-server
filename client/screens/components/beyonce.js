import React from 'react';

export default class Beyonce {
  static propTypes = {
    height: React.PropTypes.number,
    width: React.PropTypes.number,
    className: React.PropTypes.string,
  }

  render() {
    return (
      <div className={this.props.className}>
        <img
          src={`http://placebeyonce.com/${this.props.width}-${this.props.height}`}
        />
      </div>
    );
  }
}
