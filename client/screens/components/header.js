import React from 'react';
import {Link} from 'react-router';

import styles from './header.scss';

import Beyonce from '../components/beyonce';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Actions from '../../actions';

@connect( state => state )
export default class Header extends React.Component{
  static propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    user: React.PropTypes.shape({
      id: React.PropTypes.string,
      firstName: React.PropTypes.string,
    }),
  }

  render() {
    const actions = bindActionCreators(Actions, this.props.dispatch);
    return (
      <div className={styles.container}>
        <div className={styles.inner}>

          <Link className={styles.logo} to='/'>
            <img src={require('../../imgs/Acquire-Choice.png')} />
          </Link>

          <div className={styles.links}>
            <Link className={styles.link} to='/'>Home</Link>
            <Link className={styles.link} to='/rules-guidelines'>Rules and Guidelines</Link>
            <Link className={styles.link} to='/about'>About</Link>
            {!this.props.user.id ?
              <div className={styles.link} onClick={actions.fblogin}>Login</div>
            :
              <Link className={styles.link} to={`/user/${this.props.user.id}`}>Profile</Link>
            }
            {this.props.user.id &&
              <div className={styles.link} onClick={actions.logout}>Logout</div>
            }
          </div>

        </div>
      </div>
    );
  }

}
