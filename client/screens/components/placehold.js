import React from 'react';

export default class Placehold extends React.Component{
  static propTypes = {
    height: React.PropTypes.number,
    width: React.PropTypes.number,
    className: React.PropTypes.string,
  }

  render() {
    return (
      <div className={this.props.className}>
        <img
          src={`https://placehold.it/${this.props.width}x${this.props.height}`}
        />
      </div>
    );
  }
}
