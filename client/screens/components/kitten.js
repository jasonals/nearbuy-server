import React from 'react';

export default class Kitten {
  static propTypes = {
    height: React.PropTypes.number,
    width: React.PropTypes.number,
    className: React.PropTypes.string,
  }

  render() {
    return (
      <img
        className={this.props.className}
        src={`http://placekitten.com/${this.props.width}/${this.props.height}`}
      />
    );
  }
}
