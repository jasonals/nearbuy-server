import React from 'react';
import {Link} from 'react-router';

const styles = require('./home.scss');

// import Beyonce from '../components/beyonce';
import Placehold from '../components/placehold';


export default class Home extends React.Component{
  static propTypes = {
    app: React.PropTypes.shape({
      items: React.PropTypes.array,
    }),
  }

  static props = {

  }

  componentWillMount() {
    this.props.actions.getItems();
  }

  render() {
    let items = this.props.app.items;
    // console.log(this.props);
    return (
      <div className={styles.screen}>

        <Placehold className={styles.banner} width={1200} height={465} />


        <div className={styles.rowLabel}>
          <div>New</div>
        </div>
        <div className={styles.itemRow}>
          {items.map(item => {
            let picture = item.pictures.find(pic => pic.pos !== null && +pic.pos === 0);
            return (
              <Link to={`/item/${item.id}`} className={styles.item} key={item.id}>
                {picture &&
                  <img src={picture.url} />
                }
                <div className={styles.itemName}>{item.name}</div>
              </Link>
            )
          })}
        </div>
        <div className={styles.rowLabel}>
          <div>Features</div>
        </div>
        <div className={styles.itemRow}>
          {items.map(item => {
            let picture = item.pictures.find(pic => pic.pos !== null && +pic.pos === 0);
            return (
              <Link to={`/item/${item.id}`} className={styles.featuredItem} key={item.id}>
                {picture && <img src={picture.url} />}
                <div className={styles.itemName}>{item.name}</div>
              </Link>
            );
          })}
        </div>

      </div>
    );
  }
}
