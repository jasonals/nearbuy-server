import React from 'react';


import styles from './login.scss';

export default class Login {
  static propTypes = {
    processing: React.PropTypes.shape({
      doingLogin: React.PropTypes.bool,
    }),
    actions: React.PropTypes.shape({
      login: React.PropTypes.func,
      gotoRegister: React.PropTypes.func,
    }),
  }

  static props = {
    processing: {
      doingLogin: false,
    },
  };

  doLogin(e) {
    e.preventDefault();

    if (!this.props.processing.doingLogin) {
      this.props.actions.login({
        email: this.refs.email.value,
        password: this.refs.password.value,
      });
    }
  }

  gotoRegister() {
    this.props.actions.gotoRegister();
  }

  render() {
    return (
      <div className={styles.screen}>

        <div className={styles.logoWrapper}>
        </div>


        <form onSubmit={::this.doLogin} className={styles.form} noValidate>
          <div className={styles.inputWrapper}>
            <input className={styles.inputField} type='email' placeholder='Email' ref='email' disabled={this.props.processing.doingLogin}/>
            <input className={styles.inputField} type='password' placeholder='Password' ref='password' disabled={this.props.processing.doingLogin}/>
          </div>


          <button className={styles.loginButton} disabled={this.props.processing.doingLogin}>
            Log in
          </button>

        </form>

        <div className={styles.message}>Don't have an account? <span className={styles.registerLink} onClick={::this.gotoRegister}>Sign Up</span></div>
      </div>

    );
  }
}
