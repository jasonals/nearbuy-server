import React from 'react';
// import Immutable from 'immutable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Actions from '../../actions';

import Login from './login';

@connect( state => state )
export default class LoginScreen extends React.Component{

  static propTypes = {
    dispatch: React.PropTypes.func.isRequired,
  }

  render() {
    const actions = bindActionCreators(Actions, this.props.dispatch);

    return (
      <Login actions={actions} {...this.props} />
    );
  }

}
