import React from 'react';
import {Link} from 'react-router';

import styles from '../item-edit.scss';

export default class ItemEdit extends React.Component{
  static propTypes = {
    app: React.PropTypes.shape({
      items: React.PropTypes.array,
    }),
    actions: React.PropTypes.shape({
      newItem: React.PropTypes.func,
    }),
  }
  static props = {

  }
  state = {}

  create(e){
    e.preventDefault();

    this.props.actions.newItem(this.state);
  }

  render() {
    return (
      <div className={styles.screen}>
        <form onSubmit={::this.create} className={styles.form} noValidate>
          <div className={styles.inputWrapper}>
            Name
            <input className={styles.inputField} type='string' placeholder='Name' ref='name' value={this.state.name} onChange={(e) => this.setState({name: e.currentTarget.value})}/>
            Price
            <input className={styles.inputField} type='number' min={0} placeholder='Price' ref='price' value={this.state.price} onChange={(e) => this.setState({price: e.currentTarget.value})}/>
            Description
            <textarea className={styles.inputField} type='string' placeholder='Description' ref='description' rows='6' value={this.state.description} onChange={(e) => this.setState({description: e.currentTarget.value})}/>
            Active:
            <input className={styles.inputField} type='checkbox' placeholder='active' ref='active' checked={this.state.active} onChange={(e) => this.setState({active: e.currentTarget.checked})}/>
          </div>


          <button className={styles.saveButton} type='submit'>
            Save
          </button>
        </form>
      </div>
    );
  }
}
