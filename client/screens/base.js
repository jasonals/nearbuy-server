import React from 'react';

import styles from './base.scss';

import Header from './components/header';
import Footer from './components/footer';

import * as actions from '../actions';

export const Base = React.createClass({
  contextTypes: {
    router: React.PropTypes.object,
    store: React.PropTypes.object,
  },
  propTypes: {
    children: React.PropTypes.node,
  },
  // componentWillMount: function() {
  //   this.context.store.dispatch(actions.getItems());
  // },
  render() {
    // console.log(this.context);
    return (

      <div className={styles.container}>
        <Header />

        { this.props.children }


      </div>
    );
  },

});
