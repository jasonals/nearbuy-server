import React from 'react';

import styles from './guidelines.scss';


export default class Guidelines extends React.Component{
  static propTypes = {

  }

  static props = {

  };

  render() {
    return (
      <div className={styles.screen}>
        <div className={styles.inner}>
          <h2 className={styles.title}>
            Rules and Guidelines
          </h2>

          <p className={styles.p}>
            <strong>RULES</strong><br/>
            Please take note of the following:<br/>
            • The posting of pornographic material will not be tolerated. If this rule is breached, it will result in
            the immediate revoking of your membership<br/>
            • Fraudulent activity will result in the immediate revoking of your membership<br/>
            • The selling of pirated digital media will not be tolerated<br/>
            • Double posting is discouraged and multiple instances of the same ad will be reduced to one post<br/>
            • Always be courteous to members of the community<br/>
            Please remember to always be courteous to members of the Marketplace Barbados community and
            you will enjoy every second spent here.
          </p>

          <p className={styles.p}>
            <strong>SAFETY</strong><br/>
            Safety is paramount to the Marketplace Barbados team. In order to continue having a safe and enjoyable
            experience, please take note of the following guidelines:<br/>
            • Closing of Sales<br/>
            • Payment Methods<br/>
            • Avoiding Scams<br/>
            • Report Suspicious Activity
          </p>

          <p className={styles.p}>
            <strong>CLOSING OF SALES</strong><br/>
            The closing of sales is completely between the buyer and seller and Avatar Technologies( parent
            company of Marketplace Barbados ) will not be held responsible for the outcome of sales.
          </p>

          <p className={styles.p}>
            <strong>PAYMENT METHODS</strong><br/>
            Where possible, sellers please insist on payment with cash as this is the safest way to avoid being<br/>
            deceived by dishonest parties.
          </p>

          <p className={styles.p}>
            <strong>AVOIDING SCAMS</strong><br/>
            Wherever you come across an item or service with a price that is too good to be true, most of the times
            this is the case. While the Marketplace Barbados team is constantly on the lookout for scams, if you
            happen to come across such an item please report it to us. Be especially on the lookout for the following:<br/>
            • Cellphones and other electronic items at impossibly low prices<br/>
            • Apparent offers promising extremely high returns with as little effort as possible
          </p>

          <p className={styles.p}>
            <strong>REPORTING SUSPICIOUS ACTIVITY</strong><br/>
            If you encounter any activity which you consider to be suspicious, please report it to us.
          </p>

        </div>
      </div>
    );
  }
}
