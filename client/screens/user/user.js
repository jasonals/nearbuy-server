import React from 'react';
import {Link} from 'react-router';

const styles = require('./user.scss');

// import Beyonce from '../components/beyonce';
import Placehold from '../components/placehold';


export default class User extends React.Component{
  static propTypes = {
    app: React.PropTypes.shape({
      items: React.PropTypes.array,
      users: React.PropTypes.array,
    }),
    user: React.PropTypes.shape({
      id: React.PropTypes.string,
      firstName: React.PropTypes.string,
      lastName: React.PropTypes.string,
      picture: React.PropTypes.string,
    }),
    actions: React.PropTypes.shape({
      getUserByID: React.PropTypes.func,
    }),
    params: React.PropTypes.shape({
      id: React.PropTypes.string,
    }),
  }

  static props = {

  }

  componentWillMount() {
    this.props.actions.getUserByID(this.props.params.id);
  }

  render() {
    let me = this.props.params.id === this.props.user.id;
    let user = me ? this.props.user : this.props.app.users.find( _user => this.props.params.id === _user.id ) || {firstName: 'Does not exist'};
    let items = this.props.app.items.filter( item => item.owner && this.props.params.id === item.owner.id && (me ? true : item.active === true) );

    return (
      <div className={styles.screen}>
        <div className={styles.info}>

          <div className={styles.picture}>
            <img src={user.picture} />
          </div>
          <div className={styles.infoDetails}>
            <h2>{`${user.firstName} ${user.lastName}`}</h2>
          </div>

        </div>


        <div className={styles.content}>
          { me && <Link to='/item/new'>New Item</Link>}
          <div className={styles.itemRow}>
            {items.map(item => {
              let picture = item.pictures.find(pic => pic.pos !== null && +pic.pos === 0);

              return (
                <Link to={`/item/${item.id}`} className={styles.item} key={item.id}>
                  <div className={styles.itemImage}>
                    {picture && <img src={picture.url} />}
                  </div>
                  <div className={styles.itemName}>{item.name}</div>
                </Link>
              );
            })}
          </div>

        </div>


      </div>
    );
  }
}
