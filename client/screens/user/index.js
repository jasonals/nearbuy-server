import React from 'react';
// import Immutable from 'immutable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Actions from '../../actions';

import User from './user';

@connect( state => state )
export default class UserScreen extends React.Component{

  static propTypes = {
    dispatch: React.PropTypes.func.isRequired,
  }

  render() {
    const actions = bindActionCreators(Actions, this.props.dispatch);

    return (
      <User actions={actions} {...this.props} />
    );
  }

}
