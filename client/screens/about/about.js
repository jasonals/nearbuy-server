import React from 'react';

import styles from './about.scss';


export default class About extends React.Component {
  static propTypes = {

  }

  static props = {

  };

  render() {
    return (
      <div className={styles.screen}>
        <div className={styles.inner}>
          <h2 className={styles.title}>
            About US
          </h2>
          <p className={styles.p}>
            Marketplace Barbados is an online community where individuals can buy, sell and trade goods and
            services. From electronics, to cars and beauty products, you can advertise them with us and the best
            part about it is that it is completely free!
          </p>
          <p className={styles.p}>
            Advertising your products with Marketplace Barbados will allow for them to be seen by a wide audience
            due to our strong social media presence and social media integration. Marketplace Barbados’ Facebook
            sister group boasts over 7000 members!
            Come and become part of the community and buy/sell/trade with us!
          </p>
        </div>
      </div>
    );
  }
}
