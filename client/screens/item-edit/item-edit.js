import React from 'react';
import {Link} from 'react-router';
import DropZone from 'react-dropzone';

import styles from './item-edit.scss';

export default class ItemEdit extends React.Component{
  static propTypes = {
    app: React.PropTypes.shape({
      items: React.PropTypes.array,
    }),
    actions: React.PropTypes.shape({
      editItem: React.PropTypes.func,
      newItem: React.PropTypes.func,
      getItem: React.PropTypes.func,
    }),
    params: React.PropTypes.shape({
      id: React.PropTypes.string,
    }),
  }
  static props = {

  }
  state = {
    pictures: [],
  }

  componentWillMount() {
    if (this.props.params.id) {
      this.props.actions.getItem(this.props.params.id);
    }
  }

  // componentWillReceiveProps(nextProps) {
  //   this.updateItem(nextProps);
  // }
  // updateItem(props){
  //   console.log(props);
  //   let item = props.app.items.find( itm => itm.id === props.params.id ) || {};
  //
  //   this.setState({...item, ...this.state});
  // }
  edit(e){
    e.preventDefault();

    let itm = this.props.app.items.find( item => item.id === this.props.params.id ) || {};

    if (!this.state.name && !itm.name) {
      console.log('No Name');
      return;
    }

    let {owner, ...item} = this.state;
    if (itm.id) {
      this.props.actions.editItem({id: itm.id, ...item}, true);
    } else {
      this.props.actions.newItem(item);
    }
  }
  onDrop(pictures, pos){
    console.log('pictures', pictures, pos);
    this.state.pictures[pos] = pictures[0];
    this.setState({pictures: this.state.pictures});
  }
  render() {
    let item = this.props.app.items.find( itm => {
      return itm.id === this.props.params.id;
    } ) || {};

    return (
      <div className={styles.screen}>
        <form onSubmit={::this.edit} className={styles.form} noValidate>
          {item.id &&
            <Link to={`/item/${item.id}`} className={styles.back}>Back to {item.name}</Link>
          }
          <div className={styles.pictureWrapper}>
            <DropZone onDrop={(pic) => this.onDrop(pic, 0)} supportClick={false} className={styles.imgUpload} multiple={false}>
              {(this.state.pictures || item.pictures) &&
                <img src={(this.state.pictures && this.state.pictures[0] && this.state.pictures[0].preview) || (item.pictures && item.pictures[0] && item.pictures[0].url)}/>
              }
              <div>Drop image here to change</div>
            </DropZone>

            <DropZone onDrop={(pic) => this.onDrop(pic, 1)} supportClick={false} className={styles.imgUpload} multiple={false}>
              {(this.state.pictures || item.pictures) &&
                <img src={(this.state.pictures && this.state.pictures[1] && this.state.pictures[1].preview) || (item.pictures && item.pictures[1] && item.pictures[1].url)}/>
              }
              <div>Drop image here to change</div>
            </DropZone>

          </div>

          <div >
            Name
            <input className={styles.inputField} type='string' placeholder='Name' ref='name' value={this.state.name !== undefined ? this.state.name : item.name} onChange={(e) => this.setState({name: e.currentTarget.value})}/>
            Price
            <input className={styles.inputField} type='number' min={0} placeholder='Price' ref='price' value={this.state.price !== undefined ? this.state.price : item.price} onChange={(e) => this.setState({price: e.currentTarget.value})}/>
            Description
            <textarea className={styles.inputField} type='string' placeholder='Description' ref='description' rows='6' value={this.state.description !== undefined ? this.state.description : item.description} onChange={(e) => this.setState({description: e.currentTarget.value})}/>
            Active:
            <input className={styles.inputField} type='checkbox' placeholder='active' ref='active' checked={this.state.active !== undefined ? this.state.active : item.active} onChange={(e) => this.setState({active: e.currentTarget.checked})}/>
          </div>


          <button className={styles.saveButton} type='submit'>
            Save
          </button>
        </form>
      </div>
    );
  }
}
