import React from 'react';
// import Immutable from 'immutable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Actions from '../../actions';

import ItemEdit from './item-edit';

@connect( state => state )
export default class ItemEditScreen extends React.Component{

  static propTypes = {
    dispatch: React.PropTypes.func.isRequired,
  }

  render() {
    const actions = bindActionCreators(Actions, this.props.dispatch);

    return (
      <ItemEdit actions={actions} {...this.props} />
    );
  }

}
