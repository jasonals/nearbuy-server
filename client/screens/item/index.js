import React from 'react';
// import Immutable from 'immutable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Actions from '../../actions';

import Item from './item';

class ItemScreen extends React.Component{

  static propTypes = {
    dispatch: React.PropTypes.func.isRequired,
  }

  render() {
    const actions = bindActionCreators(Actions, this.props.dispatch);

    return (
      <Item actions={actions} {...this.props} />
    );
  }

}

export default connect(state => state)(ItemScreen);
