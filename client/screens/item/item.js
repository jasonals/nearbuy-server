import React from 'react';
import {Link} from 'react-router';
import cx from 'classnames';
import moment from 'moment';

const styles = require('./item.scss');

// import Beyonce from '../components/beyonce';
// import Placehold from '../components/placehold';


export default class Item extends React.Component{
  static propTypes = {
    // app: React.PropTypes.shape({
    //   items: React.PropTypes.array,
    // }),
    params: React.PropTypes.shape({
      id: React.PropTypes.string,
    }),
  }
  static props = {

  }
  state = {
    pos: 0,
  }

  componentWillMount() {
    this.props.actions.getItem(this.props.params.id);
  }

  render() {
    let item = this.props.app.items.find( itm => itm.id === this.props.params.id ) || {owner: {}};
    let picture = item.pictures && item.pictures.find(pic => pic.pos !== null && +pic.pos === this.state.pos);

    return (
      <div className={styles.screen}>
        <div className={styles.content}>

          <div className={styles.images}>
            {picture && <img src={picture.url} className={styles.image}/>}
            <div className={styles.imageList}>
              {item.pictures && item.pictures.map( picture =>
                <img src={picture.url} key={picture.url} className={styles.imageListImage} onClick={() => this.setState({pos: +picture.pos})}/>
              )}
            </div>
          </div>

          <div className={styles.info}>
            <div className={cx(styles.name, styles.block)}>
              {item.name}
              {item.owner && item.owner.id === this.props.user.id &&
                <Link to={`/item/${item.id}/edit`} className={styles.block}>&nbsp;(edit)</Link>
              }
            </div>
            {item.owner &&
              <div className={styles.block}>
                Seller:&nbsp;
                <Link to={`/user/${item.owner.id}`} className={cx(styles.owner, styles.block)}>
                  {`${item.owner.firstName} ${item.owner.lastName}`}
                </Link>
              </div>
            }

            <div className={styles.block}>Price: ${item.price ? item.price.toFixed(2) : 'n/a'}</div>
            <div className={styles.block}>Added: {moment(item.created).fromNow()}</div>
            <p className={styles.block}>{item.description}</p>
          </div>

        </div>
      </div>
    );
  }
}
