import React from 'react';
import cx from 'classnames';
import styles from './registered.scss';


export default class Registered {
  static propTypes = {
    actions: React.PropTypes.shape({
      gotoLogin: React.PropTypes.func,
    }),
  }

  gotoLogin() {
    this.props.actions.gotoLogin();
  }

  render() {
    return (
      <div className={styles.screen}>

        <i className={cx('material-icons', styles.icon)}>thumb_up</i>
        <div className={styles.message}>Account created!</div>
        <div className={styles.message}>Please check email for activation link.</div>
        <div className={styles.loginMessage}>
          Account activated? <span className={styles.loginLink} onClick={::this.gotoLogin}>Log in!</span>
        </div>

      </div>
    );
  }

}
