import React from 'react';

import {Spinner} from '../shared/spinner';

import styles from './register.scss';

export default class Register {
  static propTypes = {
    processing: React.PropTypes.shape({
      doingRegister: React.PropTypes.bool,
    }),
    actions: React.PropTypes.shape({
      register: React.PropTypes.func,
      gotoLogin: React.PropTypes.func,
    }),
  }

  doRegister(e) {
    e.preventDefault();

    if (this.props.processing.doingRegister) {
      return;
    }

    const data = {
      email: this.refs.email.value,
      password: this.refs.password.value,
      password2: this.refs.password2.value,
    };

    if (!data.password) {
      return;
    }

    if ( data.password === data.password2 ) {
      this.props.actions.register({
        email: data.email,
        password: data.password,
      });
    }

  }

  gotoLogin() {
    this.props.actions.gotoLogin();
  }

  render() {
    return (
      <div className={styles.screen}>

        <form onSubmit={::this.doRegister} className={styles.form} noValidate>

          <div className={styles.inputWrapper}>
            <input className={styles.inputField} type='email' placeholder='Email' ref='email' disabled={this.props.processing.doingRegister}/>
            <input className={styles.inputField} type='password' placeholder='Password' ref='password' disabled={this.props.processing.doingRegister}/>
            <input className={styles.inputField} type='password' placeholder='Repeat Password' ref='password2' disabled={this.props.processing.doingRegister}/>
          </div>

            { this.props.processing.doingRegister ?
              <button className={styles.registerButton} disabled={this.props.processing.doingRegister}>
                <Spinner size={40} />
              </button>
            :
              <button className={styles.registerButton} disabled={this.props.processing.doingRegister} onClick={::this.doRegister}>
                Create Account <i className='material-icons'>forward</i>
              </button>
            }
        </form>

        <div className={styles.message}>Have an account? <span className={styles.registerLink} onClick={::this.gotoLogin}>Log In</span></div>
      </div>
    );
  }
}
