import React from 'react';
// import Immutable from 'immutable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Actions from '../../actions';

import Register from './register';
import Registered from './registered';

@connect( state => state )
export default class RegisterScreen extends React.Component{

  static propTypes = {
    app: React.PropTypes.shape({
      registered: React.PropTypes.bool,
    }),
  }

  render() {
    const actions = bindActionCreators(Actions, this.props.dispatch);

    if (!this.props.app.registered) {
      return <Register actions={actions} {...this.props} />;
    } else {
      return <Registered actions={actions} {...this.props} />;
    }
  }
}
