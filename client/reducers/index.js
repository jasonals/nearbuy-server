import user from './user';
import processing from './processing';
import app from './app';
import admin from './admin';

export default {
  user,
  processing,
  app,
  admin,
};
