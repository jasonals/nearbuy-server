import * as constants from '../constants';
// import {AsyncStorage} from 'react-native';

const initialState = {
  items: [],
  users: [],
  ads: [],
};

export default function admin(state = initialState, {type, ...actionData}) {

  switch (type) {

    case constants.GET_ITEMS_SUCCESS_ADMIN:
      let items = [];

      actionData.items.forEach(item => {
        items.push({
          ...state.items.find( _item => _item.id === item.id),
          ...item,
        });
      });

      return {
        ...state,
        items,
      };

    case constants.GET_ITEM:
    case constants.UPDATE_ITEM:
      if ( state.items.find( item => item.id === actionData.item.id) ) {

        return {
          ...state,
          items: state.items.map( item => {

            if(item.id === actionData.item.id) {
              return {
                ...item,
                ...actionData.item,
              }
            }

            return item;
          }),
        }
      }
      return {
        ...state,
        items: [
          actionData.item,
          ...state.items,
        ],
      }


    case constants.GET_USERS_SUCCESS_ADMIN:
      let users = [];

      actionData.users.forEach(item => {
        users.push({
          ...state.users.find( _item => _item.id === item.id),
          ...item,
        });
      });

      return {
        ...state,
        users,
      };


    default:
      return state;
  }

}
