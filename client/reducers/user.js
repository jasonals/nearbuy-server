import constants from '../constants';

const initialState = {

};

export default function user(state = initialState, {type, ...actionData}) {
  switch (type) {
    // case constants.LOGIN_START:
    case constants.LOGIN_ERROR:
    case constants.LOGOUT_SUCCESS:
      return {
        ...initialState,
      };

    case constants.LOGIN_SUCCESS:
      return {
        ...initialState,
        ...actionData,
      };

    default:
      return state;
  }

}
