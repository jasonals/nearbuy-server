import * as constants from '../constants';
// import {AsyncStorage} from 'react-native';

const initialState = {

};

export default function processing(state = initialState, {type, ...actionData}) {

  switch (type) {
    case constants.LOGIN_START:
      return {
        ...state,
        doingLogin: true,
      };

    case constants.LOGIN_SUCCESS:
    case constants.LOGIN_ERROR:
      return {
        ...state,
        doingLogin: false,
      };

    case constants.REGISTER_START:
      return {
        ...state,
        doingRegister: true,
      };

    case constants.REGISTER_SUCCESS:
    case constants.REGISTER_ERROR:
      return {
        ...state,
        doingRegister: false,
      };

    case constants.GET_PRODUCTS_START:
      return {
        ...state,
        doingGetProducts: true,
      };

    case constants.GET_PRODUCTS_SUCCESS:
    case constants.GET_PRODUCTS_ERROR:
      return {
        ...state,
        doingGetProducts: false,
      };


    default:
      return state;
  }

}
