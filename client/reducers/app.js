import * as constants from '../constants';
// import {AsyncStorage} from 'react-native';

const initialState = {
  items: [],
  users: [],
  ads: [],
  editingItem: false,
};

export default function app(state = initialState, {type, ...actionData}) {
  switch (type) {
    case constants.GET_ITEMS:
      let items = [];

      actionData.items.forEach(item => {
        items.push({
          ...state.items.find( _item => _item.id === item.id),
          ...item,
        });
      });

      return {
        ...state,
        items,
      };

    case constants.GET_ITEM:
    case constants.NEW_ITEM_SUCCESS:
    case constants.UPDATE_ITEM:
      if ( state.items.find( item => item.id === actionData.item.id) ) {

        return {
          ...state,
          items: state.items.map( item => {

            if(item.id === actionData.item.id) {
              return {
                ...item,
                ...actionData.item,
              }
            }

            return item;
          }),
        }
      }
      return {
        ...state,
        items: [
          actionData.item,
          ...state.items,
        ],
      }


    case constants.GET_USER:
      if ( state.users.find( user => user.id === actionData.user.id) ) {

        return {
          ...state,
          users: state.users.map( user => {

            if(user.id === actionData.user.id) {
              return {
                ...user,
                ...actionData.user,
              }
            }

            return user;
          }),
        }
      }

      return {
        ...state,
        users: [
          actionData.user,
          ...state.users,
        ],
      }

    default:
      return state;
  }

}
