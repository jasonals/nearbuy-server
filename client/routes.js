import React from 'react';
import {Router, Route} from 'react-router';

import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import deepParse from '../helpers/deep-json-parse';

import reducers from './reducers';


import './css';

import routes from './routesConfig.js';


let component, store;
const reducer = combineReducers(reducers);

// get saved store data
// let previousStoreData = localStorage.getItem('store') ? deepParse(localStorage.getItem('store')) : {};
let previousStoreData = localStorage.getItem('store') ? deepParse(localStorage.getItem('store')) : {};

let HIDETOOLS = true;

if (!DEBUG || HIDETOOLS) {

  let createStoreWithMiddleware = compose(
    applyMiddleware(thunk),
    createStore
  );

  store = createStoreWithMiddleware(reducer, previousStoreData);

  component = React.createClass({
    propTypes: {
      history: React.PropTypes.object,
    },
    render() {
      return (
        <Provider store={store} >
          { () => routes(this.props.history) }
        </Provider>
      );
    },
  });

} else {

  const {devTools} = require('redux-devtools');
  const { DevTools, LogMonitor } = require('redux-devtools/lib/react');
  const DebugPanel = require('./screens/components/debugPanel').default;

  let createStoreWithDebugTools = compose(
    applyMiddleware(thunk),
    devTools(),
    createStore
  );

  store = createStoreWithDebugTools(reducer, previousStoreData);

  component = React.createClass({
    propTypes: {
      history: React.PropTypes.object,
    },
    render(){
      return (
        <div>
          <Provider store={store} >
            { () => routes(this.props.history) }
          </Provider>
          <DebugPanel top right bottom style={{maxWidth: '50%'}}>
            <DevTools store={store} monitor={LogMonitor} select={(stores) => ({...stores}) } />
          </DebugPanel>
        </div>
      );
    },
  });

}

store.subscribe( () => {
  let {user} = store.getState();

  localStorage.setItem('store', JSON.stringify({
    user,
  }));
});


export default component;
