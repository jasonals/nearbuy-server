import types from '../constants';
import query from './query';

let itemFields = `
  id, created, name, price, active
  owner { id, firstName, lastName }
  pictures { url pos }
`

export function getUserByID(id) {
  return dispatch => {
    query({
      query: `
        query item ($id: String!){
          user(id: $id) {
            id, firstName, lastName, picture
            items {
              ${itemFields}
            }
          }
        }
      `,
      variables: {id: id},
    }, (err, res) => {
      let {user: {items=[], ...user}={}} = res.data;

      dispatch({
        type: types.GET_USER,
        user: user,
      });

      dispatch({
        type: types.GET_ITEMS,
        items: items,
      });
    });
  }
}

export function getItemsAdmin() {
  return dispatch => {
    query({
      query: `query items{
        items {
          ${itemFields}
        }
      }`,
    }, (err, res) => {
      // console.log(res);
      dispatch({
        type: types.GET_ITEMS_SUCCESS_ADMIN,
        items: res.data.items || [],
      });
    });
  }
}

export function getUsersAdmin() {
  return dispatch => {
    query({
      query: `query users{
        users {
          id, firstName, lastName, email, joined
          items { id }
        }
      }`,
    }, (err, res) => {
      // console.log(res);
      dispatch({
        type: types.GET_USERS_SUCCESS_ADMIN,
        users: res.data.users || [],
      });
    });
  }
}



export function newAd({picture={}, ...variables}) {
  return (dispatch, getState) => {
    dispatch({
      type: types.NEW_AD_START,
    });

    query({
      query: `
        mutation new($name: String!, $description: String, $active: Boolean, $price: Float, $sessionToken: String!, $picture: [ItemPictureInput]) {
          ad: newAd(name: $name, description: $description, active: $active, price: $price, sessionToken: $sessionToken, pictures: $pictures) {
            id, created, name, price, description, active
            pictures { url pos }
            owner { id }
          }
        }
      `,
      variables: {...variables, sessionToken: getState().user.id},
      picture,
    }, (err, res) => {
      console.log(err, res);
      if (err) {
        dispatch({
          type: types.NEW_AD_ERROR,
        });
      } else {
        history.replaceState(null, `/admin/ad/${res.data.ad.id}`);
        dispatch({
          type: types.NEW_AD_SUCCESS,
          ad: res.data.ad,
        });
      }

    });
  };
}

export function editAd({picture={}, ...variables}, show) {
  return (dispatch, getState) => {
    query({
      query: `
        mutation edit($id: String!, $name: String, $description: String, $active: Boolean, $price: Float, $sessionToken: String, $pictures: [ItemPictureInput]) {
          ad: editAd(id: $id, name: $name, description: $description, active: $active, price: $price, sessionToken: $sessionToken, pictures: $pictures) {
            id, created, name, price, description, active
            picture { url }
            owner { id firstName lastName}
          }
        }
      `,
      variables: {...variables, sessionToken: getState().user.id},
      picture,
    }, (err, res) => {
      if (show) {
        history.replaceState(null, `/admin/ad/${res.data.ad.id}`);
      }

      dispatch({
        type: types.UPDATE_AD,
        ad: res.data.ad,
      });
    });
  };
}
