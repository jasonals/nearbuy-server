import types from '../constants';
import query from './query';


export function showBarMenu() {
  return {
    type: types.SHOW_BAR_MENU,
  };
}

export function getUserByID(id) {
  return dispatch => {
    query({
      query: `
        query item ($id: String!){
          user(id: $id) {
            id, firstName, lastName, picture
            items {
              id, created, name, price, active
              owner { id }
              pictures { url pos }
            }
          }
        }
      `,
      variables: {id: id},
    }, (err, res) => {
      let {user: {items=[], ...user}={}} = res.data;

      dispatch({
        type: types.GET_USER,
        user: user,
      });

      dispatch({
        type: types.GET_ITEMS,
        items: items,
      });
    });
  }
}

export function getItems() {
  return dispatch => {
    query({
      query: `query items{
        items(active: true) {
          id, created, name, price, active
          owner { id, firstName, lastName }
          pictures { url pos }
        }
      }`,
    }, (err, res) => {
      console.log(res);
      dispatch({
        type: types.GET_ITEMS,
        items: res.data.items || [],
      });
    });
  }
}

export function getItem(id) {
  return dispatch => {
    query({
      query: `
        query item ($id: String!){
          item(id: $id) {
            id, created, name, price, active, description
            pictures { url pos }
            owner { id, firstName, lastName }
          }
        }
      `,
      variables: {id: id},
    }, (err, res) => {
      // console.log(err, res);

      dispatch({
        type: types.GET_ITEM,
        item: res.data.item || {},
      });
    });
  }
}


export function editingItem(item) {
  return {
    type: types.EDITING_ITEM,
    item,
  };
}
