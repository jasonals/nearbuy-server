import * as user from './user';
import * as app from './app';
import * as admin from './admin';


export default {
  ...user,
  ...app,
  ...admin,
};
