import superagent from 'superagent';

export default function graphqlQuery({query, variables={}, pictures=[]}, callback){
  let request = superagent
    .post('/graphql')
    // .send({ query, variables });
    // .send(pictures);
    .field('query', query)
    .field('variables', JSON.stringify(variables) );

  if (pictures.length) {
    pictures.forEach( (file, index) => {
      // console.log(file);
      request
      .attach( 'picture', file )
      .field( 'picture', index );
    });
  }

  request.end((err, res) => {
    let {data={}, errors} = res.body;
    return callback && callback( err, {data, errors} );
  })
  .on('progress', function(e) {
    console.log('Percentage done: ', e.percent);
  });
}
