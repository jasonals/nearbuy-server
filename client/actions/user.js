import * as types from '../constants';
import query from './query';
import {history} from '../app';

// export function register({email, password}={}) {
//   return dispatch => {
//     dispatch({
//       type: types.REGISTER_START,
//     });
//   };
// }

export function fblogin() {
  return (dispatch, getState) => {
    if (!global.FB) { return; }

    dispatch({
      type: types.LOGIN_START,
    });

    global.FB.login( ({authResponse, status}) => {
      // console.log(authResponse, status);
      if (status !== 'connected') {
        return dispatch({
          type: types.LOGIN_ERROR,
        });
      }

      query({
        query: `
          mutation login($accessToken: String, $signedRequest: String, $userID: String ) {
            me: fblogin(accessToken: $accessToken, signedRequest: $signedRequest, userID: $userID ) {
              id, firstName, lastName, email, picture
            }
          }
        `,
        variables: authResponse,
      }, (err, res) => {
        if (res.data.errors) {
          return dispatch({
            type: types.LOGIN_ERROR,
            errors: res.data.errors,
          });
        }

        dispatch({
          type: types.LOGIN_SUCCESS,
          ...res.data.me,
        });
      });

    }, {scope: 'public_profile,email'} );
  }
}

export function logout() {
  return {
    type: types.LOGOUT_SUCCESS,
  };
}

export function newItem({pictures=[], ...variables}) {
  return (dispatch, getState) => {
    dispatch({
      type: types.NEW_ITEM_START,
    });

    query({
      query: `
        mutation new($name: String!, $description: String, $active: Boolean, $price: Float, $sessionToken: String!, $pictures: [ItemPictureInput]) {
          item: newItem(name: $name, description: $description, active: $active, price: $price, sessionToken: $sessionToken, pictures: $pictures) {
            id, created, name, price, description, active
            pictures { url pos }
            owner { id }
          }
        }
      `,
      variables: {...variables, sessionToken: getState().user.id},
      pictures,
    }, (err, res) => {
      console.log(err, res);
      if (err) {
        dispatch({
          type: types.NEW_ITEM_ERROR,
        });
      } else {
        history.replaceState(null, `/item/${res.data.item.id}`);
        dispatch({
          type: types.NEW_ITEM_SUCCESS,
          item: res.data.item,
        });
      }

    });
  };
}

export function editItem({pictures=[], ...variables}, showItem) {
  return (dispatch, getState) => {
    query({
      query: `
        mutation edit($id: String!, $name: String, $description: String, $active: Boolean, $price: Float, $sessionToken: String, $pictures: [ItemPictureInput]) {
          item: editItem(id: $id, name: $name, description: $description, active: $active, price: $price, sessionToken: $sessionToken, pictures: $pictures) {
            id, created, name, price, description, active
            pictures { url pos }
            owner { id firstName lastName}
          }
        }
      `,
      variables: {...variables, sessionToken: getState().user.id},
      pictures,
    }, (err, res) => {
      if (showItem) {
        history.replaceState(null, `/item/${res.data.item.id}`);
      }

      dispatch({
        type: types.UPDATE_ITEM,
        item: res.data.item,
      });
    });
  };
}
