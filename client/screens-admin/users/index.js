import React from 'react';
// import Immutable from 'immutable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {Link} from 'react-router';
import moment from 'moment';

import Actions from '../../actions';

import styles from './style.scss';

class User extends React.Component {
  newTab(e) {
    e.preventDefault();
    console.log(e);
    window.open(e.target.href);
  }

  render() {
    return (
      <tr className={styles.item}>
        <td>
          <Link to={`/user/${this.props.id}`} target='_blank' onClick={(e) => this.newTab(e)}>{this.props.firstName}</Link>
        </td>
        <td>
          <Link to={`/user/${this.props.id}`} target='_blank' onClick={(e) => this.newTab(e)}>{this.props.lastName}</Link>
        </td>
        <td>{this.props.email}</td>
        <td>{moment(this.props.joined).fromNow()}</td>
        <td className={styles.number}>{(this.props.items || []).length}</td>
      </tr>
    );
  }
}


class Users extends React.Component{

  componentWillMount() {
    this.props.actions.getUsersAdmin();
  }

  render() {

    return (
      <div className={styles.container}>
        <table className={styles.table}>
          <thead>
            <tr>
              <th>First name</th>
              <th>Last name</th>
              <th>Email</th>
              <th>Joined</th>
              <th className={styles.number}>Items</th>
            </tr>
          </thead>
          <tbody>
            {this.props.admin.users.map(user => <User {...user} key={user.id}/>)}
          </tbody>
        </table>
      </div>
    );
  }

}

export default connect(
  state => state,
  dispatch => ({actions: bindActionCreators(Actions, dispatch)})
)(Users);
