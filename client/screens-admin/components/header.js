import React from 'react';
import {Link} from 'react-router';

import styles from './header.scss';

import { connect } from 'react-redux';


@connect( state => state )
export default class Header extends React.Component{
  static propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    user: React.PropTypes.shape({
      id: React.PropTypes.string,
      firstName: React.PropTypes.string,
    }),
  }

  render() {
    return (
      <div className={styles.container}>
        <div className={styles.inner}>

          <Link className={styles.logo} to='/'>
            <img src={require('../../imgs/Acquire-Choice.png')} />
          </Link>

          <div className={styles.links}>
            <Link className={styles.link} to='/'>Public</Link>
            <Link className={styles.link} to='/admin/ads'>Ads</Link>
            <Link className={styles.link} to='/admin/users'>Users</Link>
            <Link className={styles.link} to='/admin/items'>Items</Link>
            <Link className={styles.link} to='/admin/permissions'>Permissions</Link>
          </div>

        </div>
      </div>
    );
  }

}
