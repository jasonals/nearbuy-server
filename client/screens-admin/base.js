import React from 'react';

import styles from './base.scss';

import Header from './components/header';


export default class AdminBase extends React.Component {
  static propTypes = {
    children: React.PropTypes.node,
  }
  render() {
    return (

      <div className={styles.container}>
        <Header />
        <div style={{overflow: 'auto', flex: 1}}>
          { this.props.children }
        </div>
      </div>
    );
  }

}
