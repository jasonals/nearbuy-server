import React from 'react';
// import Immutable from 'immutable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {Link} from 'react-router';
import moment from 'moment'
import numeral from 'numeral'

import Actions from '../../actions';

import styles from './style.scss';

class Item extends React.Component {
  active() {
    this.props.editItem({id: this.props.id, active: !this.props.active})
  }

  newTab(e) {
    e.preventDefault();
    console.log(e);
    window.open(e.target.href);
  }
  render() {
    console.log(this.props.owner);
    return (
      <tr className={styles.item}>
        <td>
          <Link target='_blank' to={`/item/${this.props.id}`} onClick={(e) => this.newTab(e)}>{this.props.name}</Link>
        </td>
        <td><input checked={this.props.active} type='checkbox' onChange={() => this.active()}/></td>

        <td>{moment(this.props.created).fromNow()}</td>

        <td>
          {this.props.owner &&
            <Link target='_blank' to={`/user/${this.props.owner.id}`} onClick={(e) => this.newTab(e)}>
              {`${this.props.owner.firstName} ${this.props.owner.lastName}`}
            </Link>
          }
        </td>

        <td className={styles.number}>{numeral(this.props.price).format('$0,0.00')}</td>

        <td className={styles.number}>{(this.props.pictures || []).length}</td>
      </tr>
    );
  }
}


class Items extends React.Component{

  componentWillMount() {
    this.props.actions.getItemsAdmin();
  }

  render() {
    return (
      <div className={styles.container}>
        <table className={styles.table}>
          <thead>
            <tr>
              <th>Name</th>
              <th>Active</th>
              <th>Created</th>
              <th>Owner</th>
              <th className={styles.number}>Price</th>
              <th className={styles.number}>Pics</th>
            </tr>
          </thead>
          <tbody>
            {this.props.admin.items.map(item => <Item {...item} editItem={this.props.actions.editItem} key={item.id}/>)}
          </tbody>
        </table>
      </div>
    );
  }

}

export default connect(
  state => state,
  dispatch => ({actions: bindActionCreators(Actions, dispatch)})
)(Items);
