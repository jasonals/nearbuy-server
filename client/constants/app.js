import keymirror from 'keymirror';


export default keymirror({
  GET_USER: null,
  GET_ITEMS: null,
  GET_ITEM: null,

  NEW_ITEM_START: null,
  NEW_ITEM_SUCCESS: null,
  NEW_ITEM_ERROR: null,

  UPDATE_ITEM: null,
  UPDATE_ITEMS: null,
});
