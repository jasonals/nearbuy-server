import keymirror from 'keymirror';

let constants = {};

[
  'GET_USERS',
  'GET_ITEMS',
  'GET_ADS',
].forEach(str => {
  constants[str+'_START_ADMIN'] = null;
  constants[str+'_SUCCESS_ADMIN'] = null;
  constants[str+'_ERROR_ADMIN'] = null;
});

export default keymirror(constants);
