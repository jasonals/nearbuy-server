import keymirror from 'keymirror';

export default keymirror({
  LOGIN: null,
  LOGIN_SUCCESS: null,
  LOGIN_ERROR: null,

  REGISTER: null,
  REGISTER_SUCCESS: null,
  REGISTER_ERROR: null,

  LOGOUT: null,
});
