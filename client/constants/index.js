import * as user from './user';
import * as app from './app';
import * as navigator from './navigator';
import * as admin from './admin';

export default {
  ...user,
  ...app,
  ...navigator,
  ...admin,
};
