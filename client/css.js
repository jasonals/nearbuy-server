require('./styles/roboto/roboto.css');
require('./styles/materialicons/material-icons.css');

require('./styles/bootstrap-import.scss');
require('./styles/reset-flex.scss');
// require('./styles/font-awesome-import.scss');
// require('./styles/app.scss');
require('./styles/global.scss');
require('./styles/buttons.scss');
require('./styles/input.scss');
