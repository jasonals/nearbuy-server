import React from 'react';
import {Router, Route} from 'react-router';

import {Base} from './screens/base';
import Home from './screens/home';
import About from './screens/about';
import Guidelines from './screens/guidelines';

import Item from './screens/item';
import ItemEdit from './screens/item-edit';
import Login from './screens/login';
import User from './screens/user';

import AdminBase from './screens-admin/base';
import AdminItems from './screens-admin/items';
import AdminUsers from './screens-admin/users';
import AdminAds from './screens-admin/ads';


export default function routes (history) {
  return (
    <Router history={history}>
      <Route component={Base}>
        <Route path='/' component={Home} />
        <Route path='items' component={Home} />
        <Route path='item/new' component={ItemEdit} />
        <Route path='item/:id' component={Item} />
        <Route path='item/:id/edit' component={ItemEdit} />
        <Route path='user/:id' component={User} />
        <Route path='login' component={Login} />

        <Route path='about' component={About} />
        <Route path='rules-guidelines' component={Guidelines} />

      </Route>

      <Route path='admin' component={AdminBase}>
        <Route path='items' component={AdminItems} />
        <Route path='users' component={AdminUsers} />
        <Route path='ads' component={AdminAds} />
        <Route path='permissions' component={Home} />

      </Route>
    </Router>
  );
}
