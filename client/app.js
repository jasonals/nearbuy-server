require('babel-core/polyfill');


import React from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/lib/createBrowserHistory';
import Routes from './routes';

export let history = createHistory();

ReactDOM.render( <Routes history={history}/>, document.getElementById('app') );
